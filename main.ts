import { Logger } from "tslog";
import * as Excel from 'exceljs';
import commandLineArgs from 'command-line-args';

// ------------------- CONSTS ------------------- //
const CELL_REGEX = /(\D+)(\d+)/;
const COLUMNS_NAMES = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
const COLORS_LEVELS = [ '69B34C','FAB733', 'FF4E11'];


// ------------------- LOGGING ------------------- //
const log: Logger = new Logger();

// ------------------- ARGUMENTS ------------------- //
const optionDefinitions = [
    { name: 'input', alias: 'i', type: String, multiple: true },
    { name: 'cells', alias: 'c', type: String, multiple: true, defaultValue: [] },
    { name: 'output', alias: 'o', type: String, defaultValue: "out.xlsx" },
    { name: 'worksheet', alias: 'w', type: String, defaultValue: "1" },
    { name: 'identifiers', alias: 'd', type: String, multiple: true, defaultValue: [] },
];
type Options = {
    input: string[]; 
    cells: string[]; 
    output: string; 
    worksheet: string;
    identifiers: string[];
};

// ------------------- HELPERS ------------------- //
function indexToColumnName(input: number, pow: number = 0, acc: string[] = []): string {
    if(input === 0) return acc.join('');
    const position = (input) % Math.pow(COLUMNS_NAMES.length, pow + 1) / Math.pow(COLUMNS_NAMES.length, pow) ||  COLUMNS_NAMES.length;
    const toSubstract = position * Math.pow(COLUMNS_NAMES.length, pow); 
    acc.unshift(COLUMNS_NAMES[position - 1]);
    return indexToColumnName(input - toSubstract, pow + 1, acc)
}
function columnNameToIndex(input: string): number {
    return input.split('').reverse().reduce((acc, curr, index) => {
        return acc + (COLUMNS_NAMES.indexOf(curr) + 1) * Math.pow(COLUMNS_NAMES.length, index);
    }, 0);
}
function parseCellsRange(input: string): string[] {
    const rangeSplit = input.toUpperCase().split(':');
    if(rangeSplit.length === 1) { return [input.toUpperCase()]}
    const startCell = rangeSplit[0];
    const endCell = rangeSplit[1];
    const startCol = startCell.match(CELL_REGEX)![1];
    const startRow = startCell.match(CELL_REGEX)![2];
    const endCol = endCell.match(CELL_REGEX)![1];
    const endRow = endCell.match(CELL_REGEX)![2];
 
    const colDiff = columnNameToIndex(endCol) - columnNameToIndex(startCol); 
    const rowDiff = parseInt(endRow) - parseInt(startRow);
    const colDirection = colDiff > 0 ? +1 : -1;
    const rowDirection = rowDiff > 0 ? +1 : -1;

    const cols = Array.from(new Array(Math.abs(colDiff) + 1)).map((_, index) => {
        return indexToColumnName(columnNameToIndex(startCol) + colDirection * index);
    })
    const rows = Array.from(new Array(Math.abs(rowDiff) + 1)).map((_, index) => {
        return parseInt(startRow) + rowDirection * index;
    });
    
    const cells = rows.map((row) => {
        return cols.map((col) => {
            return col + row;
        });
    }).flat(1); 

    return cells;
}
function getColorForDiff(diffCount: number): string {
    switch(diffCount){
        case 0: 
            return COLORS_LEVELS[0];
        case 1: 
            return COLORS_LEVELS[1];
        default: 
            return COLORS_LEVELS[2];
    }
}

// ------------------- MAIN ------------------- //
async function main (){  
    log.info('Usage:', 'npm run main -- -i AQ.xlsx AQ.xlsx -c E5:N8 E14:N19 E22:N41 E43:N53 E55:N71 E73:N102 E104:N105 -w Requirements -d S L');

    // Parse arguments
    const options = commandLineArgs(optionDefinitions) as Options;
    log.debug('Program running with options: ', options);

    log.info('Loading workbooks in memory');
    const workbooks = await Promise.all(options.input.map((it) => {
        const workbook = new Excel.Workbook();
        return workbook.xlsx.readFile(it);
    }))
    log.info(`Loaded ${workbooks.length} workbooks into memory`);
    
    const worksheet = options.worksheet.match(/^[0-9]+$/) ? parseInt(options.worksheet) : options.worksheet;
    const identifiers = options.identifiers;

    
    const outputWorkbook = new Excel.Workbook();
    const outputWorksheet = outputWorkbook.addWorksheet("output");

    const cellsToCheck = options.cells.map((it) => parseCellsRange(it)).flat(1);
    cellsToCheck.forEach((cell) => {
        log.info(`Computing cell: ${cell}`);

        let values = workbooks.map((workbook) => {
            return '' + workbook.getWorksheet(worksheet).getCell(cell).value ;
        });
        const diffValues = [...new Set(values)];

        if(identifiers.length === values.length){
            values = values.map((value, index) => {
                return identifiers[index] + ':' + value;
            })
        }
        outputWorksheet.getCell(cell).value = values.join(' / ');
        outputWorksheet.getCell(cell).fill = {
            type: 'pattern',
            pattern:'solid',
            fgColor:{argb:getColorForDiff(diffValues.length - 1)}
          };

    })

    log.info(`Saving result to: ${options.output}`);
    await outputWorkbook.xlsx.writeFile(options.output);

}

main();
