# XLSX Comparator 

This little tool allow you to compare *x* version of the "same" Excel file. 

## Setup 

```sh 
git clone https://gitlab.com/dcylabs/xlsx-compare
cd xlsx-compare
npm i  
```

## Usage 

```txt
npm run main -- -i A.xlsx B.xlsx -c A4 B5 A8:B10
    
    --input         -i      Excel files to compare: A.xlsx B.xlsx
    --cells         -c      Cells to compare: A4 B5 A8:B10  
    --output        -o      Output file: out.xlsx
    --worksheet     -w      Worksheet to compare: 1 2 Costs 
    --identifiers   -i      Identifiers for versions: V1 V2
```
 